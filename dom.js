// $(document).ready(function(){
//     console.log("hiding!");
//     $('header').hide();
// })

//request = message
//.bug-grid-row



console.log("fogbugz detected");

    var platforms       = {};
    var elementsToHide  = [];
    var changedElements = [];
    var hiddenElements  = [];

function start(){
    
    platforms       = {};
    elementsToHide  = [];
    changedElements = [];
    hiddenElements  = [];

    window.timeLimit = 20000;
    window.time = 0;

    if(window.wait) return;
    
    window.wait = setInterval(function () {
        var caseCount = ($(".list-group-size").length>0) ? $(".list-group-size").text().match(/\d+/g) : false; //get cases count
        var loading2 = $("#info-bar").attr("style") ? ($("#info-bar").attr("style").match("display: none") ? false : true) : false; //check if loading div still showing

        if(!caseCount) return;
        //console.log($(".bug-grid").children("tbody").children().length +" :: " + caseCount);
        console.log($(".loading-progress-bar").length < 1, $(".bug-grid").children("tbody").children().length >= caseCount, !loading2)
        if ($(".loading-progress-bar").length < 1 && $(".bug-grid").children("tbody").children().length >= caseCount && !loading2) {
            clearInterval(wait);
            window.wait=undefined;
            console.log("LOADED" + $(".bug-grid").children("tbody").children().length);
            setTimeout(function () {
                init();
            }, 100);
    
        }
        time+=100;
        if(time>timeLimit) {clearInterval(wait);window.wait=undefined;};
    }, 100);
}


function init() {
    //chrome.storage.sync.remove("prefs");
    chrome.storage.sync.get("prefs", (data) => {
        if ($.isEmptyObject(data)) console.log("no configuration found");
        else {
            console.log("configuration found");
            platforms = data.prefs;
            scrap();
            hide();
        }
    });
}


function scrap() {
    elementsToHide = [];
    var title = "";
    $(".bug-grid-row").each((id, el) => {
        title = $(el).find(".grid-title-link").attr("title") || "";
        if(title=="") console.error("EXTENSION:" + el + "is invalid")
        for (var platform in platforms) {
            if (platforms[platform].hide && title.match(new RegExp(platform, 'i'))) {
                elementsToHide.push({
                    element:el,
                    id:$(el).attr("data-ix-bug")
                });
                break;
            }
        }
    });

    console.log("elements found that need to be hidden: " + elementsToHide.length);
}

function hide() {
     
    //$(hiddenElements).fadeIn();
    // for(var fkey in hiddenElements){
    //     for(var skey in elementsToHide){
    //         if(!$(elementsToHide[skey]).is($(hiddenElements[fkey]))){
    //             $(hiddenElements[skey]).fadeIn();
    //         }
    //     }
    // }
    $(hiddenElements).each((fkey, fVal)=>{
        var contains = false;
        $(elementsToHide).each((skey, sVal)=>{
            if(fVal.id == sVal.id){
                contains = true;
            }
        })
        if(!contains) $(fVal.element).fadeIn(); 
    })

    elementsToHide.forEach((val) => {
        $(val.element).fadeOut(1000);
    })

    setTimeout(function(){
        elementsToHide.forEach((val)=>{
            $(val.element).attr("style","display:none");
        })
    },1000)

    hiddenElements = elementsToHide;
}

function show(){
    
}

function highlightSelected() {
    //first reset style
    //debugger;
    changedElements.forEach((val) => {
        var val1 = $(val).attr("style");
        if(!val1) return;
        $(val).attr("style", val1.replace("background-color:red;", ""));
    })
    changedElements = [];

    elementsToHide.forEach((val) => {
        var val1 = $(val.element).attr("style") || "";
        $(val.element).attr(
            "style", val1+";background-color:red;"
        );
        changedElements.push(val.element);
    })
    console.log(changedElements);
}


chrome.extension.onMessage.addListener((request, sender, sendResponse) => {
    
    if(request.urlchange){
        setTimeout(function(){
            start();
        },500)
        
    }
    else if (request.hide) {
        platforms = request.platforms
        scrap();
        hide();
        chrome.storage.sync.set({
            "prefs": platforms
        });
    } else if (request.showSelected) {
        platforms = request.platforms
        scrap();
        highlightSelected();
    } else {
        sendResponse({});
    }
    console.log(request);

});