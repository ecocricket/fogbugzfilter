var platforms = {};
var defaultPlatforms = {
    "Android":{},
    "ios"    :{},
    "linux"  :{},
    "mac"    :{},
    "windows":{},
    "xbox"   :{},
    "ps4"    :{},
    "VR"     :{}
};
// setInterval(function(){
//     console.log($(".platform"));
// },1000);
//chrome.storage.sync.remove("prefs");
$(window).ready(init);

function init(){
    
    chrome.storage.sync.get("prefs", function(data){
        //get length of prefs
        
        if($.isEmptyObject(data)){
            console.log("no data is present or length doesnt mach");
            setData();
            generateHTML();
            applyStyleToPopup();
            console.log(platforms);
        } 
        else{
            console.log(data);
            platforms = data.prefs;
            generateHTML();
            applyStyleToPopup();
        }
         //now that dynamic content has been added
         addEventListenersToButtons();
    })
   

}

function generateHTML(){
    for (var key in platforms) {
        if (platforms.hasOwnProperty(key)) {
            $(".list-group").append(
            '<button type="button" value="'+key+'" title="'+key+'" class="list-group-item platform">'+(key.length>15 ? key.substr(0,15) + "..." : key)+'\
                <a style="float:right;padding:5px;line-height: 0.5;" class="btn btn-danger removeFilter">&times</a>\
            </button>');
        }
    }
}

function applyStyleToPopup(){
    //iterate through buttons
    $(".platform").each((id, el)=>{
        //check if button checked in platform
        //console.error(platforms);
        if(platforms[$(el).attr("value")].hide){
            $(el).toggleClass("list-group-item-danger");
        }
    })
}
//if data is not present just set it all to false
function setData(){
    for (var key in defaultPlatforms) {
        if (defaultPlatforms.hasOwnProperty(key)) {
            platforms[key] = {
                hide:false
            };
        }
    }

    chrome.storage.sync.set({
        "prefs":platforms
    });
}

function sendMessage(data, callback){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, data, function(response) {
            callback(response);
        });
    });
}

function handleResponse(response){
    console.log(response);
}

function addEventListenersToButtons(){
    $("body").on("click", ".platform", selectPlatformEvent);
    $("body").on("click", ".removeFilter", removePlatformEvent);
}

function selectPlatformEvent(e){
    console.log("sending::selected");
    var $el = $(e.currentTarget);
    platforms[$el.attr("value")].hide = platforms[$el.attr("value")].hide? false : true;
    var data = {
        showSelected:true,
        hide:false,
        platforms:platforms
    };
    console.log("data " , data);
    sendMessage(data,(response) => {
        console.log("response: "+ response);
    });

    $(e.currentTarget).toggleClass("list-group-item-danger");
}

function removePlatformEvent(e){
    e.stopPropagation();
    $(e.currentTarget).parent().fadeOut(500);
    setTimeout(()=>{
        $(e.currentTarget).parent().remove();
    },500);

    delete platforms[$(e.currentTarget).parent().attr("value")];
    console.log("deleted " + $(e.currentTarget).parent().attr("value"));
    console.log(platforms);
    chrome.storage.sync.set({
        "prefs":platforms
    });
}


$("#hideBtn").click(()=>{
    console.error("click");
    console.warn(platforms);
    var data = {
        showSelected:true,
        hide:true,
        platforms:platforms
    };

    sendMessage(data,handleResponse)
});

$("#addItemBtn").click(()=>{
    var $inputItem = $('<input type="text" style="background:#66D7D1;" class="list-group-item pre-platform" placeholder="Write filter here"/>');
    $(".list-group").append($inputItem);
    $(".pre-platform").off("keydown");
    $(".pre-platform").keydown(function(ev){
        if(ev.key == "Enter"){
            console.log("key down enter");
            var val = $(this).val();
            $(this).remove();
            $(".list-group").append(
            '<button type="button" value="'+val+'" title="'+val+'" class="list-group-item platform">'+(val.length>15 ? val.substr(0,15) + "..." : val)+'\
                <a style="float:right;padding:5px;line-height: 0.5;" class="btn btn-danger removeFilter">&times</a>\
            </button>');
            
            //button is added, now add it to platforms and save it
            platforms[val] = {
                hide:false
            };
            
            chrome.storage.sync.set({
                "prefs":platforms
            });
        }
    })
})
//console.error("hiding!");
// chrome.storage.sync.set({'value':"heyy"},function(){
//     console.log("saved!");
// })